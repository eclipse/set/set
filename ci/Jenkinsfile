pipeline {
  agent {
    kubernetes {
      yamlFile 'ci/kubernetes.yaml'
    }
  }

  triggers {
    upstream threshold: hudson.model.Result.UNSTABLE, upstreamProjects: (BRANCH_NAME == 'main' ? 'toolboxmodel/main, browser/main' : '')
  }

  options {
    timeout(time: 30, unit: 'MINUTES')
    buildDiscarder(logRotator(numToKeepStr: '10'))
    disableConcurrentBuilds()
  }

  stages {
    stage('Build About') {
      steps {
        container('hugo') {
          dir('web/about') {
            sh 'hugo'
            deployFolder src: 'public', dest: '../../java/bundles/org.eclipse.set.feature/rootdir/web/about/'
          }
        }
      }
    }

    stage('Build NodeJS') {
      environment {
        GITHUB_TOKEN = credentials('github-bot-token')
      }

      steps {
        container('nodejs') {
          dir('web/textviewer') {
            npmBuildTo target: '../../java/bundles/org.eclipse.set.feature/rootdir/web/textviewer/'
          }
          dir('web/pdf') {
            script {
              def version = sh(returnStdout: true, script: 'node getVersion.js').trim()
              downloadFile file: "pdfjs.zip", url: "https://ci.eclipse.org/set/job/pdfjs-cache/lastSuccessfulBuild/artifact/pdfjs-${version}-dist.zip"
              npmBuildTo target: '../../java/bundles/org.eclipse.set.feature/rootdir/web/pdf/'
            }
          }
        }
      }
    }

    stage('Build Java') {
      steps {
        container('maven') {
          mvn goal: 'clean verify checkstyle:checkstyle -P sign,signexe', dependencies: 'maven.deps'
          archiveArtifacts artifacts: 'java/bundles/org.eclipse.set.releng.set.product/target/products/SET-*.zip'
          archiveArtifacts artifacts: 'java/bundles/org.eclipse.set.releng.repository/target/*.zip'
        }
      }
    }
    
    stage('Process Licenses') {
      steps {
        container('maven') {
          collectDependencies()
        }
      }
    }

    stage('Deploy Snapshot') {
      when {
        anyOf {
          buildingTag()
          branch 'main'
          branch pattern: 'release/[0-9.]+', comparator: 'REGEXP'
        }
      }
      steps {
        deployP2Site name: "set", branch: BRANCH_NAME, path: 'java/bundles/org.eclipse.set.releng.repository/target/repository'
        deployProduct name: 'set', branch: BRANCH_NAME, path: 'java/bundles/org.eclipse.set.releng.set.product/target/products/SET-*.zip'
      }
    }
  }
  post {
    always {
      // Publish JUnit test result
      junit testResults: 'java/bundles/*/target/surefire-reports/*.xml', allowEmptyResults: true
      handleIssues()
    }
  }
}
